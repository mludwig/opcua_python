# generic opcua-server AS exploration and dumping into a csv
# test with a caen venus sim engine
# exercise a simulated venus combo


# ---start asycio---
import asyncio
import sys
import os
import time


sys.path.insert(0, "..")
import logging
from asyncua import Client, Node, ua
from datetime import datetime

from clientinput import *
#from asyncua import *

# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
# logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.WARNING)
_logger = logging.getLogger('asyncua')
# ---end asyncio

# ---common---
# these get injected via clientinput.py
#endpoint = "opc.tcp://ccc7:4841"
#crate="simSY4527"

globalName = "opcuaserver" 
#globalUrn = "urn:JCOP:OpcUaCaenServer"
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
 
output_fn="output.txt"
error_fn="error.txt"


# -- start asyncio functions---

# get value from a var name like 'TEST3_CycleTime_AS.PosSt.value'
async def get( cl, varname ):
    var=cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value


# read to get the variant type and then set
async def set( cl, parname, valueToSet ):
    _logger.debug("set-get %s", parname)
    par=cl.get_node( parname )

    dvx=await par.get_data_value()    # need the type of the variant
    dv = ua.DataValue(ua.Variant( valueToSet, dvx.Value.VariantType))
    dv.ServerTimestamp = None         # needed, otherwise .ua will not accept
    dv.SourceTimestamp = None
    await par.set_value(dv)
    _logger.debug("set %s to %s", parname, valueToSet)

# extract "Views" from "QualifiedName(NamespaceIndex=0, Name='Views')"
async def cleanNodeName (nn):
    n0 = nn.replace("QualifiedName", "").replace(")","").replace("(","")
    #print("cleanNodeName n0= ", n0)
    n1 = n0.split(",")[1]
    #print("cleanNodeName n1= ", n1)
    n2 = n1.replace("Name=","").replace("'","")
    #print("cleanNodeName n2= ", n2)
    return( n2 )       

# get the value of the dataValue. If there is no dv, return false
async def nodeVarDV(node):
    
    print("nodeVarDV node= ", node)
    #"I_crate": nodePrefix+"genericPilot0.I_crate",
    #i_crate = await get( client, asdict["I_crate"] )

    #async def get( cl, varname ):
    #var = cl.get_node( varname )
    #_logger.debug("get var %s", var )
    #value=await var.read_value()
    #_logger.debug("get %s= %r", varname, value)
    #return value

    
    
    #no, this needs to be rewritten. api has changed. look into verticalSliceBasic.py
    #nodeclass = await node.get_node_class()
    nodeclass = await node.read_node_class()
    print("p0 nodeVarDV node= ", node, " nodeclass= ", nodeclass) # "NodeClass.ObjectType"
    # only look into nodes which have this search string, i.e. crate
    if ( str(node).find( crate ) == -1):
        return( False )
          
    if ( str(nodeclass).find("NodeClass.Variable") == -1):
        print("NodeClass.Variable not found in ", nodeclass)
        return( False )
    else:    
        print("p00 NodeClass.Variable found in ", nodeclass)
        # https://python-opcua.readthedocs.io/en/latest/node.html
        # Get value of a node as a DataValue object. Only variables (and 
        # properties) have values. An exception will be generated for 
        # other node types. DataValue contain a variable value as a 
        # variant as well as server and source timestamps
        try:
            print("p010 try getting a type from node= ", node)
            dt = await node.get_data_type()
            print("p1x nodeVarDV node= ", node, " dt= ", dt)

            print("p01 try getting a dv from node= ", node)
            dv = await node.get_data_value()
            print("p1 nodeVarDV node= ", node, " dv= ", dv)
            return(dv)
        except:
            with open( error_fn, "a") as e:
                print("p11 failed getting a dv from node= ", node)

                # why does this give TWO lines?!
                emsg=str("nodeVarDV node= ")+str(node)+str(" returns no dataValue\n")
                e.write( emsg )   
            return( False )
        
# get the value of a node        
async def nodeVarValue(cl, nodename):
    print("nodeVarValue nodename= ", nodename )
    
    
    if ( str(nodename).find("Views") != -1 ):
        print("nodeVarValue nodename= ", nodename, " is Views, no type" )
        return("no_type")  # Views have no type
    if ( str(nodename).find("Objects") != -1 ):
        print("nodeVarValue nodename= ", nodename, " is Objects, no type" )
        return("no_type")  # Views have no type
    if ( str(nodename).find("Server") != -1 ):
        print("nodeVarValue nodename= ", nodename, " is Server, no type" )
        return("no_type")  # Views have no type
    if ( str(nodename).find("StandardMetaData") != -1 ):
        print("nodeVarValue nodename= ", nodename, " is StandardMetaData, no type" )
        return("no_type")  # Views have no type

    var = cl.get_node( nodename )
    print("nodeVarValue var= ", var )   

    value = await var.read_value()
    print("nodeVarValue value= ", value )

    #dv = await nodeVarDV(node)
    #print("nodeVarValue dv= ", dv )
    #if (dv):
    #    return(dv.Value.Value)
    #else:
    return("no_value")
      
# get the type of a node
async def nodeVarType(cl, nodename):
    print("nodeVarType nodename= ", nodename )
    

    if ( str(nodename).find("Views") != -1 ):
       print("nodeVarType nodename= ", nodename, " is Views, no type" )
       return("no_type")  # Views have no type
    if ( str(nodename).find("Objects") != -1 ):
       print("nodeVarType nodename= ", nodename, " is Objects, no type" )
       return("no_type")  # Views have no type
    if ( str(nodename).find("Server") != -1 ):
       print("nodeVarType nodename= ", nodename, " is Server, no type" )
       return("no_type")  # Views have no type
    if ( str(nodename).find("StandardMetaData") != -1 ):
       print("nodeVarType nodename= ", nodename, " is StandardMetaData, no type" )
       return("no_type")  # Views have no type


    var = cl.get_node( nodename )
    print("nodeVarType var= ", var )  
    #attribute IDs see  https://reference.opcfoundation.org/v104/Core/docs/Part6/A.1/
    # i.e. DataType = 14, Vale = 13
    #attr = ua.AttributeIds.Value
    #attr = ua.AttributeIds.DataType
    attr = ua.AttributeIds.NodeId
    var_att = await var.read_attribute( attr )
    print("nodeVarType var_att= ", var_att )   
    
    attr1 = ua.AttributeIds.NodeClass
    var_att1 = await var.read_attribute( attr1 )
    print("nodeVarType var_att1= ", var_att1)   
  
    
    
    
    value = await var.read_value()
    print("nodeVarValue value= ", value )

    #dv = await nodeVarDV(node)
    #print("nodeVarType dv= ", dv )
    #if (dv):
    #    return(str(dv.Value.VariantType).replace("VariantType.", ""))
    #else:
    return("no_type")

async def nodeAccess(cl, node):
    #print("nodeAccess node= ", node )
    al = await node.get_access_level()
    #print("nodeAccess node= ", node, " al= ", al)
    # {<AccessLevel.CurrentRead: 0>, <AccessLevel.CurrentWrite: 1>}
    accessLevel=str("")
    if ( str(al).find("AccessLevel.CurrentRead") != -1 ):
        accessLevel += str("R")
    if ( str(al).find("AccessLevel.CurrentWrite") != -1 ):
        accessLevel += str("W")
    return(accessLevel)

async def cleanNSIndex(nname):
    if ( str(nname).find("NamespaceIndex=0") > 0):
        return(0)
    if ( str(nname).find("NamespaceIndex=1") > 0):
        return(1)
    if ( str(nname).find("NamespaceIndex=2") > 0):
        return(2)
    
    
    
# ---start asyncio main---
async def main():
    _logger.info("===start init===")

    start_time = time.time()

    # read endpoint and crate from client_input.py, which gets injected by docker cp
    # we don't delete this file after each run
    print("have included and executed once clientinput.py (inject it to set endpoint and crate)")
    print("it should look like:\nendpoint = 'opc.tcp://ccc7:4841'\ncrate = 'simSY4527'\n")
    print("endpoint= ", endpoint )
    print("crate= ", crate )
    try:
        os.remove( output_fn )
        os.remove( error_fn )
    except:
        print("could not remove oputput.txt and/or error.txt")

    f = open( output_fn, "w")
    e = open( error_fn, "w")
    datetime_object = datetime.now()
    ostr=str("# ") + str(datetime_object) + str("\n")
    f.write( ostr) 
    e.write( ostr) 
    ostr=str("# ") + str(endpoint) + str(" ") + str(crate) + str("\n")
    f.write( ostr) 
    e.write( ostr) 
    ostr="# errors: dump AS from a running OPCUA server and read all dv values once (avoid exception if value not published) \n"
    f.write( ostr)
    e.write( ostr) 
    ostr=str("# author: michael.ludwig@cern.ch  (c) CERN\n#\n") 
    f.write( ostr)
    e.write( ostr) 

    

    async with Client(url=endpoint) as client:

        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        _logger.debug("root node= %r", root)
        nsidx = await client.get_namespace_index( uri )
        _logger.debug("namespace index= %d", nsidx)
        nodePrefix=f"ns={nsidx};s="
        _logger.info("nodePrefix= %s", nodePrefix)
        _logger.info("===end init===")

        
        # Node class methods, see: https://python-opcua.readthedocs.io/en/latest/node.html
        # discover the complete AS: drill top-down, 5 levels
        # root(0)->objects(1)->crate(2)->board(3)->channel(4)->subtypes(5)
        _logger.warning("children of root= %r", await client.nodes.root.get_children())
        nodes0 = await client.nodes.root.get_children()
        _logger.warning("nodes0= %r", nodes0 )
        
        for node0 in nodes0:
            print("node0= ", node0)
            name0 = str(await node0.read_browse_name())
            print("raw name0= ", name0)
            name0 = await cleanNodeName(name0) 
            print("cleaned name0= ", name0 )
            type0 = await nodeVarType(client, name0)
            value0 = await nodeVarValue(client, name0)
            #type0 = await nodeVarType(client, node0)
            #value0 = await nodeVarValue(client, node0)
            
            print("point00 type0= ", type0, " value0= ", value0)
            if ( type0 != "no_type" and value0 != "no_value" ):
                access0 = await nodeAccess(client, node0)
                print("name0= ", name0, " type= ", type0, " value= ", value0, " access0= ", access0)   
                f.write("name0= ", name0, " type= ", type0, " value= ", value0, " access0= ", access0) + str("\n")   
             
            nodes1 = await node0.get_children()
            for node1 in nodes1:
                #name1 = str(await node1.get_browse_name())
                name1 = str(await node1.read_browse_name())
                print("point0 name1= ", name1)
                namespaceIndex = await cleanNSIndex(name1)
                print("point0 namespaceIndex= ", namespaceIndex)
                
                # we drop all non Object subnodes for now: server and opcua stuff
                if namespaceIndex != 2:
                    print("drop namespace= ", namespaceIndex)
                    continue
                                
                name1 = name0 + "." + await cleanNodeName(name1)
                type1 = await nodeVarType(client, node1)
                value1 = await nodeVarValue(client, node1)
                print("point01 type1= ", type1, " value1= ", value1)

                if ( type1 != "no_type" and value1 != "no_value" ):
                    access1 = await nodeAccess(node1)
                    print("name1= ", name1, " type= ", type1, " value= ", value1, " access= ", access1)             
                    ostr=str(name1) + str(", ") + str(value1) + str(", ") + str(type1) + str(", ") + str(access1) + str("\n")             
                    f.write( ostr )             
 
                nodes2 = await node1.get_children()
                for node2 in nodes2:
                    #name2 = str(await node2.get_browse_name())
                    name2 = str(await node2.read_browse_name())
                    print("point1 name2= ", name2)

                    name2 = name1 + "." + await cleanNodeName(name2)
                    #print("full name2= ", name2 )
                    type2 = await nodeVarType(client, node2)
                    value2 = await nodeVarValue(client, node2)
                    print("point11 type2= ", type2, " value2= ", value2)
                    if ( type2 != "no_type" and value2 != "no_value" ):
                        access2 = await nodeAccess(client, node2)
                        print("name2= ", name2, " type= ", type2, " value= ", value2, " access= " ,access2)
                        ostr=str(name2) + str(", ") + str(value2) + str(", ") + str(type2) + str(", ") + str(access2) + str("\n")             
                        f.write( ostr )             

                    nodes3 = await node2.get_children()
                    for node3 in nodes3:
                        #name3 = str(await node3.get_browse_name())
                        name3 = str(await node3.read_browse_name())
                        print("point2 name3= ", name3)

                        name3 = name2 + "." + await cleanNodeName(name3)
                        #print("full name3= ", name3 )
                        type3 = await nodeVarType(client, node3)
                        value3 = await nodeVarValue(client, node3)
                        if ( type3 != "no_type" and value3 != "no_value" ):
                            access3 = await nodeAccess(client, node3)
                            print("name3= ", name3, " type= ", type3, " value= ", value3, " access= " , access3)             
                            ostr=str(name3) + str(", ") + str(value3) + str(", ") + str(type3) + str(", ") + str(access3) + str("\n")            
                            f.write( ostr )             
        
                        nodes4 = await node3.get_children()
                        for node4 in nodes4:
                            #name4 = str(await node4.get_browse_name())
                            name4 = str(await node4.read_browse_name())
                            #print("point3 name4= ", name4)

                            name4 = name3 + "." + await cleanNodeName(name4)
                            #print("full name4= ", name4 )
                            type4 = await nodeVarType(client, node4)
                            value4 = await nodeVarValue(client, node4)
                            
                            if ( type4 != "no_type" and value4 != "no_value" ):
                                access4 = await nodeAccess(client, node4)
                                print("name4= ", name4, " type= ", type4, " value= ", value4, " access= ", access4)
                                ostr=str(name4) + str(", ") + str(value4) + str(", ") + str(type4) + str(", ") + str(access4) + str("\n")             
                                f.write( ostr )             
                                    
                            # any more sub-objects ? etc etc
                            #nodes5 = await node4.get_children()
                            #print("children of node4= %r", nodes5 )
                            #_logger.warning("children of node4 %r = %r", node4, nodes5 )


        # After one second we exit the Client context manager - this will close the connection.
        f.close()
        e.close()
        print("took ", time.time() - start_time, " sec" )
        print("output is in file ", output_fn)
        print("errors are in file ", error_fn)
        await asyncio.sleep(1)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.run_until_complete(main())
    loop.close()

# ---end asyncio main---


