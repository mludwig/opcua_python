# demo client to read/write to an OPCUA server to perform lxi basic tests
# talks to the opcua lxi port:23600 server which should run locally
# inject into docker gitlab-registry.cern.ch/mludwig/opcua_python:opcua-asyncio.cs8.1.0.0

#
# see api at:
# https://python-opcua.readthedocs.io/en/latest/client.html
# v0.9.92 https://github.com/FreeOpcUa/opcua-asyncio.git
import asyncio
import sys
import os
import time
import logging

sys.path.append("./")
sys.path.append("/usr/bin/python3")

from asyncua import Node, Client, ua
from datetime import datetime

# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('asyncua')

# ---common---
endpoint0 = "opc.tcp://localhost:23600"

# globalName = "opcuaserver" 
# globalUrn = "OPCUASERVER"
# uri = 'OPCUASERVER'
# open6:
# globalName = "OpcUaLxiServer" 
# globalUrn = "open62541.server.application"
uri = 'OPCUASERVER'
# ------------
 

# -- asyncio functions---

# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class SubscriptionHandler:
    """
    The SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        #_logger.info('datachange_notification %r %s', node, val)
        _logger.warning("new data arrived: %r= %s", node, val)



# read a scalar, list, anything
async def get( cl, varname ):
    var = cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value

# set a scalar Int32
async def setInt32( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Int32 )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a scalar Float
async def setFloat( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)





# set a scalar Boolean
async def setBool( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Boolean )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a python list as floats into AS
async def setListFloat( cl, parname, listToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", listToSet)
    parnode = cl.get_node( parname )
    #uav = ua.Variant( listToSet, ua.VariantType.Float, ua.ValueRank.OneDimension )
    uav = ua.Variant( listToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= ", uav)
    await parnode.set_value( uav )


# ---start asyncio main---
async def main():
    _logger.info("===start init===")
    async with Client(url=endpoint0) as client:
        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        _logger.debug("root node= %r", root)
        nsidx = await client.get_namespace_index( uri )
        _logger.debug("namespace index= %d", nsidx)
        nodePrefix=f"ns={nsidx};s="
        _logger.info("nodePrefix= %s", nodePrefix)
        _logger.info("===end init===")

        # define a dictionary for the OPCUA Adress Space
        asdict = {
            "test1.TargetVoltageCh0": nodePrefix+"plh250_test1.Chan001.TargetVoltage",
            "test2.TargetVoltageCh0": nodePrefix+"plh250_test2.Chan001.TargetVoltage",
            "test3.TargetVoltageCh0": nodePrefix+"pl303qmd_test3.Chan001.TargetVoltage",
            "test3.TargetVoltageCh1": nodePrefix+"pl303qmd_test3.Chan002.TargetVoltage"
             }
        
        # ramp one channels using pilot. Set defaults, off etc, wait for ramping down (we dont know how long this takes


        for x in range(100):
            volts = x/10 + 3.0
            _logger.info("writing %f %s %s", volts, " to ", asdict["test1.TargetVoltageCh0"])
            await setFloat( client, asdict["test1.TargetVoltageCh0"], volts )

            _logger.info("writing %f %s %s", volts, " to ", asdict["test2.TargetVoltageCh0"])
            await setFloat( client, asdict["test2.TargetVoltageCh0"], volts )

            _logger.info("writing %f %s %s", volts, " to ", asdict["test3.TargetVoltageCh0"])
            await setFloat( client, asdict["test3.TargetVoltageCh0"], volts )

            _logger.info("writing %f %s %s", volts, " to ", asdict["test3.TargetVoltageCh1"])
            await setFloat( client, asdict["test3.TargetVoltageCh1"], volts )

            await asyncio.sleep(10)

        await setFloat( client, asdict["test1.TargetVoltageCh0"], 2.0 )
        await setFloat( client, asdict["test2.TargetVoltageCh0"], 2.0 )
        await setFloat( client, asdict["test3.TargetVoltageCh0"], 2.0 )
        await setFloat( client, asdict["test3.TargetVoltageCh1"], 2.0 )
        print("end")

if __name__ == '__main__':
    asyncio.run(main())
# ---end asyncio main---

