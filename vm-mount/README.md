# README.md - vm-mount

this directory is mounted bidirectionally by the podman container. The container looks for a file
/vm-mount/client.py, picks it up, runs it once and deletes it.

therefore you only need to drop your OPCUA python scripts into this (very special) directory, and name ./client.py the one to execute.
Your script - client.py - will disappear ("consumed - in the true sense") and be executed. 

All other files are left untouched - also this README.md.



