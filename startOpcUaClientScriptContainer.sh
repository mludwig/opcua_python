#!/bin/bash
if [ -z "$1" ]
then
	image=gitlab-registry.cern.ch/mludwig/opcua_python:opcua-asyncio.cal9
else
	image=$1
fi 
echo "image= "$image
podman run --net=host --expose=4841 --expose=4891 --expose=4901 --expose=8880 --expose=8881 --expose=23400 --expose=23500 --expose=23600 --expose=24911 --volume ./vm-mount:/vm-mount:rw,Z $image

