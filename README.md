python clients which can read/write/subscribe to OPCUA servers for both .ua and .open6 toolkits
===============================================================================================

opcua-asyncio  [ https://github.com/FreeOpcUa/opcua-asyncio ] is a succesor to OPCUA-python [ https://github.com/FreeOpcUa/python-opcua ].


opcua-asyncio offers
--------------------

* asynchronous management (similar to multistasking on a single CPU core), on top of mostly non-threadsafe python 
  code (not multi-threaded, rather concurrent multi-task) locks and semaphores are available

* the concurrent and asynchronous structure is great for networking "and related primitives". coroutines are
  callback methods. async/await is part of python 3.something, asyncio just exploits this for OPCUA

* main event loop and callback structures naturally evolve into fully asynchronous GUIs. Main 
  loop scheduling characteristics adapto to the underlying OS (and can also be explicitly managed). 
  Therefore there is big hope that this works for native windows as well

* decent subscription and event handler mechanisms for OPCUA AS: many nodes can be listened to 
  in one subscription, which then calls one handler-type with many handler instances. One node can 
  trigger several handler as well, so it is M(nodes):N(handlers)

* coroutines are "object generators", and the objetcs are executes in various contexts, i.e. the main loop.
  normally coroutines are "chained": main calls coroutine1 calls coroutine2 etc... like in good old GUI programming

* can wait for "future objects" to be finished, and then trigger. O.M.G. the whole python world is jumping at me

* license is LGPL: completely free contagious license, suits CERN

code is at
----------

https://github.com/FreeOpcUa/opcua-asyncio.git (just clone it and surf through the examples)

further articles and info on asyncio
------------------------------------

https://docs.python.org/3/library/asyncio-dev.html
https://www.datacamp.com/community/tutorials/asyncio-introduction
https://djangostars.com/blog/asynchronous-programming-in-python-asyncio/

https://github.com/FreeOpcUa/opcua-asyncio/issues/

how to run it
=============

Start up a combo
----------------
podman run --network=host -e "SIMOPT1=-shorthand" -e "SIMOPT2=-cfg" -e "SIMCONFIG=sim_caen_default.xml" --expose=8880 --expose=23400 gitlab-registry.cern.ch/industrial-controls/services/qa/qatoolbox/venus-caen:venus-caen-combo.cal9
- or qaToolbox/venus-caen/runComboContainer.sh

run the client image
--------------------
- The image has everything you need on top of alma9 for doing OPCUA via python

podman run --net=host --expose=4841 --expose=4901 --expose=8890 gitlab-registry.cern.ch/mludwig/opcua_python:opcua-asyncio.cal9

- You can inject your script into the running container to get it executed (once):
docker cp myscript.py containerID:/client.py

- the container looks for the existence of the /client.py, and if the file is found it is executed once and then deleted from the container.

- moost scripts connect to localhost, but the scripts can be changed of course: run anywhere




