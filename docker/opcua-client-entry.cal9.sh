#!/bin/bash
#
# check versions
python3 --version
pip3 install asyncua
pip3 install asyncio
pip3 install dataclasses
#
echo "looking for script to execute "
echo "inject a script like: podman cp mytest0.py <mycontainerID>:/vm-mount/client.py or copy into /vm-mount/client.py"
while true
	do
	if [ -f /vm-mount/client.py ]
	then
	    echo "/vm-mount/client.py file exists, execute it once"
	    python3 /vm-mount/client.py
	    echo "/vm-mount/client.py done, removing from container."
            rm /vm-mount/client.py
            echo "==="
            echo "inject a script like: podman cp mytest0.py mycontainer:/vm-mount/client.py"
            echo "==="
	fi
        sleep 1
done


