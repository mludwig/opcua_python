# demo client to read/write to an OPCUA server to perform venus basic tests
# talks to the opcua venus pilot server, which runs on a VM
# inject into docker gitlab-registry.cern.ch/mludwig/opcua_python:opcua-asyncio.cs8.1.0.0

#
# see api at:
# https://python-opcua.readthedocs.io/en/latest/client.html
# v0.9.92 https://github.com/FreeOpcUa/opcua-asyncio.git
import asyncio
import sys
import os
import time
import logging

sys.path.append("./")
sys.path.append("/usr/bin/python3")

from asyncua import Node, Client, ua
from datetime import datetime

# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('asyncua')

# ---common---
endpoint0 = "opc.tcp://fdvs-venus-caen0:23400"
globalName = "opcuaserver" 
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
# ------------
 

# -- asyncio functions---

# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class SubscriptionHandler:
    """
    The SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        #_logger.info('datachange_notification %r %s', node, val)
        _logger.warning("new data arrived: %r= %s", node, val)



# read a scalar, list, anything
async def get( cl, varname ):
    var = cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value

# set a scalar Int32
async def setInt32( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Int32 )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a scalar Boolean
async def setBool( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Boolean )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a python list as floats into AS
async def setListFloat( cl, parname, listToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", listToSet)
    parnode = cl.get_node( parname )
    #uav = ua.Variant( listToSet, ua.VariantType.Float, ua.ValueRank.OneDimension )
    uav = ua.Variant( listToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= ", uav)
    await parnode.set_value( uav )


# ---start asyncio main---
async def main():
    _logger.info("===start init===")
    async with Client(url=endpoint0) as client:
        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        _logger.debug("root node= %r", root)
        nsidx = await client.get_namespace_index( uri )
        _logger.debug("namespace index= %d", nsidx)
        nodePrefix=f"ns={nsidx};s="
        _logger.info("nodePrefix= %s", nodePrefix)
        _logger.info("===end init===")

        # define a dictionary for the OPCUA Adress Space
        asdict = {
            "I_crate": nodePrefix+"genericPilot0.I_crate",
            "I_slot": nodePrefix+"genericPilot0.I_slot",
            "I_channel": nodePrefix+"genericPilot0.I_channel",
            "allChannelsLoad": nodePrefix+"genericPilot0.allChannelsLoad",
            "rampUpAllBoardChannels": nodePrefix+"genericPilot0.rampUpAllBoardChannels",
            "powerAllChannels": nodePrefix+"genericPilot0.powerAllChannels"
            }
        
        # ramp all channels using pilot. Set defaults, off etc, wait for ramping down (we dont know how long this takes
        await setInt32( client, asdict["I_crate"], 0 )
        await setInt32( client, asdict["I_channel"], 0 )
        i_crate = await get( client, asdict["I_crate"] )
        _logger.info("got I_crate= %d", i_crate)
        i_channel = await get( client, asdict["I_channel"] )
        _logger.info("got I_channel= %d", i_channel)

        await setListFloat( client, asdict["allChannelsLoad"], [1000.0, 1.0e-6] )
        
        # ramps per board: slot 0,3,5
        # we could also do this in one go, generic:
        #     await setListFloat( client, asdict["rampUpAllChannels"], rampParams )
        # but we choose to do this per board
        rampParams = [100.0, 20, 5, 5] 
        await setInt32( client, asdict["I_slot"], 0 )
        i_slot = await get( client, asdict["I_slot"] )
        _logger.info("got I_slot= %d, ramping up this board", i_slot)
        await setListFloat( client, asdict["rampUpAllBoardChannels"], rampParams )
        await setInt32( client, asdict["I_slot"], 3 )
        i_slot = await get( client, asdict["I_slot"] )
        _logger.info("got I_slot= %d, ramping up this board", i_slot)
        await setListFloat( client, asdict["rampUpAllBoardChannels"], rampParams )
        await setInt32( client, asdict["I_slot"], 5 )
        i_slot = await get( client, asdict["I_slot"] )
        _logger.info("got I_slot= %d, ramping up this board", i_slot)
        await setListFloat( client, asdict["rampUpAllBoardChannels"], rampParams )

        # ramp down if needed
        await setBool( client, asdict["powerAllChannels"], False )
        _logger.info("ramping down with parameters= %s", rampParams )
        await asyncio.sleep(20)


        # ramp up, wait a minute
        await setBool( client, asdict["powerAllChannels"], True)
        _logger.info("ramping up with parameters= %s", rampParams )
        await asyncio.sleep(20)

        # switch off = ramp down again
        await setBool( client, asdict["powerAllChannels"], False )
        _logger.info("ramping down with parameters= %s %s", rampParams, " and finish" )
        
        # After one second we exit the Client context manager - this will close the connection.
        await asyncio.sleep(1)
        print("end")

if __name__ == '__main__':
    asyncio.run(main())
# ---end asyncio main---

