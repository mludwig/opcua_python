# client to read/write to a venus pilot OPCUA server to perform
# the consistency test as specified in:
# https://readthedocs.web.cern.ch/display/ICKB/test_fwCaen_CaenEventModeCounter0
#
# talks to the opcua venus pilot server, which runs on a VM
# inject into docker gitlab-registry.cern.ch/mludwig/opcua_python:opcua-asyncio.cs8.1.0.0
#
# see api at:
# https://python-opcua.readthedocs.io/en/latest/client.html
# v0.9.92 https://github.com/FreeOpcUa/opcua-asyncio.git
import asyncio
import sys
import os
import time
import logging

sys.path.append("./")
sys.path.append("/usr/bin/python3")

from asyncua import Node, Client, ua
from datetime import datetime

# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
logging.basicConfig(level=logging.INFO)
#logging.basicConfig(level=logging.DEBUG)
_logger = logging.getLogger('asyncua')

# ---common---
# pilot
endpoint0 = "opc.tcp://fdvs-venus-caen0:23400"
#endpoint0 = "opc.tcp://localhost:23400"
# caen
endpoint1 = "opc.tcp://fdvs-venus-caen0:4901"
# endpoint1 = "opc.tcp://localhost:4901"
globalName = "opcuaserver" 
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
# ------------
 

# -- asyncio functions---

# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class SubscriptionHandler:
    """
    The SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        #_logger.info('datachange_notification %r %s', node, val)
        _logger.warning("new data arrived: %r= %s", node, val)



# read a scalar, list, anything
async def get( cl, varname ):
    var = cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value

# set a scalar Int32
async def setInt32( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Int32 )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a scalar Float
async def setFloat( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a scalar Boolean
async def setBool( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Boolean )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a python list as floats into AS
async def setListFloat( cl, parname, listToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", listToSet)
    parnode = cl.get_node( parname )
    #uav = ua.Variant( listToSet, ua.VariantType.Float, ua.ValueRank.OneDimension )
    uav = ua.Variant( listToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= ", uav)
    await parnode.set_value( uav )

# stage0: prepare the test
# full parameter call set for  completeness, always all channels 0...11
async def stage0_pilot( nodePrefix, cpilot, pilotGenericName, pilotScadaName ):
    print("stage0_pilot start")

    dictPilot = {
        "VSEL": nodePrefix+pilotGenericName+".VSEL",
        "ISEL": nodePrefix+pilotGenericName+".ISEL",
        "allChannelsLoad": nodePrefix+pilotGenericName+".allChannelsLoad"
    }

    # unmute, don't count== normal
    node = nodePrefix+pilotScadaName+".scadaGlobalMute"
    await setBool( cpilot, node, 0 )
    node = nodePrefix+pilotScadaName+".scadaCounterFlag"
    await setBool( cpilot, node, 0 )

    # pilot: set noise, loads all channels
    await setBool( cpilot, dictPilot["VSEL"], 0 )
    await setBool( cpilot, dictPilot["ISEL"], 0 )
    await setListFloat( cpilot, dictPilot["allChannelsLoad"], [ 1e6, 1e-6 ] )
    
    await asyncio.sleep( 30 ) # leave time for the caen opc to reconnect if needed

    print("stage0_pilot end")

# acquire the nb of channels of a given board of a caen crate
async def caen_boardNbChannels( ccaen, crateName, b ):
        nsidx0 = await ccaen.get_namespace_index( uri )
        nodePrefix0=f"ns={nsidx0};s="
        bStr = f'{b:02d}'
        nodeNbChannels = nodePrefix0+crateName+".Board"+bStr+".NrOfCh"
        nbOfChannels = await get( ccaen, nodeNbChannels )
        return nbOfChannels



# set up , reset all channels to a good status
async def stage0_caen( nodePrefix, ccaen, crateName, boardList ):
    print("stage0_caen start")
    v0set = 19.8
    v1set = 200.0
    i0set = 0.1
    i1set = 1.0
    rup = 5.0
    rdown = 5.0
        
    # prepare caen: all channels V0Set, V1Set, I0Set, I1Set
    for b in boardList:
        bStr = f'{b:02d}'
        
        nbOfChannels = await caen_boardNbChannels( ccaen, crateName, b) 
        #nodeNbChannels = nodePrefix0+crateName+".Board"+bStr+".NrOfCh"
        #nbOfChannels = await get( ccaen, nodeNbChannels )
        print("stage0_caen: nbOfChannels= ", nbOfChannels, " for Board", bStr)

        for c in range( nbOfChannels ):
            cStr = f'{c:03d}'
            node = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".Pw"
            await setInt32( ccaen, node, 0 )
            _logger.debug("stage0_caen: set Pw=0 for ", node)
            
            node = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".V0Set"
            await setFloat( ccaen, node, v0set )
            _logger.debug("stage0_caen: set V0Set= ", v0set," for ", node)

            node = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".V1Set"
            await setFloat( ccaen, node, v1set )
            _logger.debug("stage0_caen: set V1Set= ", v1set," for ", node)

            node = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".I0Set"
            await setFloat( ccaen, node, i0set )
            _logger.debug("stage0_caen: set I0Set= ", i0set," for ", node)

            node = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".I1Set"
            await setFloat( ccaen, node, i1set )
            _logger.debug("stage0_caen: set I1Set= ", i1set," for ", node)

            node = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".RUp"
            await setFloat( ccaen, node, rup )
            _logger.debug("stage0_caen: set rup= ", rup," for ", node)
            
            node = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".RDWn"
            await setFloat( ccaen, node, rdown )
            _logger.debug("stage0_caen: set rdown= ", rdown," for ", node)

    # wait for all channels showing 0. we could theoreticaly poll and wait for them per channel, as done in stage8
    await asyncio.sleep(30)
    for b in boardList:
        bStr = f'{b:02d}'
        nbOfChannels = await caen_boardNbChannels( ccaen, crateName, b) 

        for c in range(nbOfChannels):
            bStr = f'{b:02d}'
            cStr = f'{c:03d}'
            nodeVmon = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".VMon"
            vmon = await get( ccaen, nodeVmon )            
            assert vmon == 0, " channel is not at zero"
            
            nodeStatus = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".Status"
            status = await get( ccaen, nodeStatus )            
            assert status == 0, " channel status not 0"
           
    print("stage0_caen end")

# mute scada counters, reset them
async def stage1_2_pilot( nodePrefix, cpilot, pilotGenericName, pilotScadaName ):
    print("stage1_pilot start")
    
    node = nodePrefix+pilotScadaName+".scadaGlobalMute"
    await setBool( cpilot, node, 1 )

    node = nodePrefix+pilotScadaName+".scadaCounterFlag"
    await setBool( cpilot, node, 1 )

    # need several times so that the pilot picks it up reliably. not so nice, but works for now.
    node = nodePrefix+pilotScadaName+".scadaGlobalReset"
    await setBool( cpilot, node, 1 )
    await asyncio.sleep(2)
    await setBool( cpilot, node, 1 )
    await asyncio.sleep(2)
    await setBool( cpilot, node, 1 )
    
    # for 500 nodes wait 15 secs
    print("stage1_pilot wait 10 sec for crate to unsubscribe so that crate is assumed dead")
    await asyncio.sleep(10)
    
    # flips back to false
    scadaGlobalReset = await get( cpilot, node )
    assert scadaGlobalReset == False, "reset counters did not work"
    
    await asyncio.sleep(5)
    print("stage1_2_pilot end")

# read counter arrays and check that they are zero
async def stage3_pilot( nodePrefix, cpilot, pilotGenericName, pilotScadaName ):
    print("stage3_pilot start")
    
    node = nodePrefix+pilotScadaName+".scadaGlobalCounter"
    globalCounter = await get( cpilot, node )
    print("stage3_pilot: globalCounter= ", globalCounter )
    assert globalCounter == 0, "global counter is not zero after reset"
 
    node = nodePrefix+pilotScadaName+".counters"
    counterArray = await get( cpilot, node )
    #print("stage3_pilot: counters= ", counterArray )
    
    sum = 0
    for i in range( len(counterArray )):
        sum += counterArray[ i ]
        
    assert sum == 0, "counter array is not zero after reset"

    print("stage3_pilot end")

# start test, unmute
async def stage4_pilot( nodePrefix, cpilot, pilotGenericName, pilotScadaName ):
    print("stage4_pilot start: start and unmute")
    
    node = nodePrefix+pilotScadaName+".scadaGlobalMute"
    await setBool( cpilot, node, 0 )

    node = nodePrefix+pilotScadaName+".scadaCounterFlag"
    await setBool( cpilot, node, 1 )
    await asyncio.sleep(5)
    print("stage4_pilot end: start and unmute OK")
    
# ramp 1st part, using Pw=ON and VSEL, ISEL==0    
async def stage5_pilot( nodePrefix, cpilot, pilotGenericName, pilotScadaName ):
    print("stage5_pilot start: ramp1")
    
    node = nodePrefix+pilotGenericName+".VSEL"
    await setBool( cpilot, node, 0 )
    node = nodePrefix+pilotGenericName+".ISEL"
    await setBool( cpilot, node, 0 )
    print("stage5_pilot end: ramp1 OK")


# set Pw == 1 all channels
# all boards have 12 channels we assume
async def stage5_caen( nodePrefix, ccaen, crateName, boardList ):
    print("stage5_caen start: ramp1")
   
    for b in boardList:
        bStr = f'{b:02d}'
        nbOfChannels = await caen_boardNbChannels( ccaen, crateName, b) 
        for c in range(nbOfChannels):
            bStr = f'{b:02d}'
            cStr = f'{c:03d}'
            node = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".Pw"
            await setInt32( ccaen, node, 1 )
            # _logger.info("stage0_caen: set Pw= for ", node)
            #print("stage5_caen: set Pw= 1 for ", node)
    print("stage5_caen end: ramp1 started OK")

# first plateau: check that all are here
async def stage6_caen( nodePrefix, ccaen, crateName, boardList ):
    print("stage6_caen start: plateau1")
    
    await asyncio.sleep( 20 )

    for b in boardList:
        bStr = f'{b:02d}'
        nbOfChannels = await caen_boardNbChannels( ccaen, crateName, b) 
        for c in range(nbOfChannels):
            cStr = f'{c:03d}'
            nodeVmon = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".VMon"
            nodeV0Set = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".V0Set"
            vmon = await get( ccaen, nodeVmon )
            v0set = await get( ccaen, nodeV0Set )
            
            trycounter = 100
            while ((vmon < v0set * 0.95) and (trycounter > 1000)) :
                vmon = await get( ccaen, nodeVmon )
                #_logger.info("stage6_caen: board= ", b, " channel= ", c, " waiting for plateau1 vmon=", vmon)
                print("stage6_caen: board= ", b, " channel= ", c, " waiting for plateau1 vmon=", vmon, " (try ", trycounter, ")")
                trycounter -= 1
            
            #_logger.info("stage6_caen: board= ", b, " channel= ", c, " has reached plateau1 vmon=", vmon)
            print("stage6_caen: board= ", b, " channel= ", c, " has reached plateau1 vmon=", vmon)
    
    # stay a bit on the plateau for noise studies
    print("stage6_caen end: plateau1 reached, staying there for 30secs ")
    await asyncio.sleep( 30 )
    print("stage6_caen end: plateau1 reached OK")

# ramp 2nd part, VSEL, ISEL==1    
async def stage7_pilot( nodePrefix, cpilot, pilotGenericName, pilotScadaName ):
    print("stage7_pilot start: ramp2")
    
    node = nodePrefix+pilotGenericName+".VSEL"
    await setBool( cpilot, node, 1 )
    node = nodePrefix+pilotGenericName+".ISEL"
    await setBool( cpilot, node, 1 )
    print("stage7_pilot end: ramp2 started OK")

# second plateau: check that all are here
async def stage8_caen( nodePrefix, ccaen, crateName, boardList ):
    print("stage8_caen start: plateau2")
    
    # wait for the first channel to be there already
    b = boardList[0]
    c = 0
    bStr = f'{b:02d}'
    cStr = f'{c:03d}'
    nodeVmon = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".VMon"
    nodeV1Set = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".V1Set"
    vmon = await get( ccaen, nodeVmon )
    v1set = await get( ccaen, nodeV1Set )
    while vmon < v1set * 0.95 :
        vmon = await get( ccaen, nodeVmon )
        #_logger.info("stage8_caen: board= ", b, " channel= ", c, " waiting for plateau2 vmon=", vmon)
        print("stage8_caen: board= ", b, " channel= ", c, " waiting for plateau2 vmon=", vmon)
        await asyncio.sleep( 2 )
        
    #_logger.info("stage8_caen: board= ", b, " channel= ", c, " has reached plateau2 vmon=", vmon, " checking all other channels")
    print("stage8_caen: board= ", b, " channel= ", c, " has reached plateau2 vmon=", vmon, " checking all other channels")
       
    for b in boardList:
        bStr = f'{b:02d}'
        nbOfChannels = await caen_boardNbChannels( ccaen, crateName, b)
        
        flagDone = False
        while flagDone == False:
            for c in range(nbOfChannels):
                flagDone = True

                cStr = f'{c:03d}'
                nodeVmon = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".VMon"
                nodeV1Set = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".V1Set"
                vmon = await get( ccaen, nodeVmon )
                v1set = await get( ccaen, nodeV1Set )
            
                if vmon < v1set * 0.95 :
                    flagDone = False
                    print("stage8_caen: board= ", b, " channel= ", c, " still ramping vmon=", vmon)
                    
                await asyncio.sleep( 1 )
        
        # this board has reached plateau2, treat next board
    
    # all boards have finished
    # stay a bit on the plateau for noise studies
    print("stage8_caen end: all channels have reached plateau2, staying there for 30secs ")
    await asyncio.sleep( 30 )
    print("stage8_caen end: plateau2 reached OK")


# set Pw == 0 all channels
# all boards have 12 channels we assume
async def stage9_caen( nodePrefix, ccaen, crateName, boardList ):
    print("stage9_caen start: Pw off")
    
    for b in boardList:
        bStr = f'{b:02d}'
        nbOfChannels = await caen_boardNbChannels( ccaen, crateName, b) 
        for c in range(nbOfChannels):
            cStr = f'{c:03d}'
            node = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".Pw"
            await setInt32( ccaen, node, 0 )
            _logger.debug("stage0_caen: set Pw= for ", node)
            
    # we should wait quite long for a complete ramp down        
    await asyncio.sleep( 200 )

    # check we are down
    for b in boardList:
        for c in range(12):
            bStr = f'{b:02d}'
            cStr = f'{c:03d}'
            nodeVmon = nodePrefix+crateName+".Board"+bStr+".Chan"+cStr+".VMon"
            vmon = await get( ccaen, nodeVmon )
            assert vmon == 0, "stage9_caen channel not at zero"
            
    print("stage9_caen end: back to zero OK")

# stop, put pback to normal    
async def stage10_pilot( nodePrefix, cpilot, pilotGenericName, pilotScadaName ):
    print("stage10_pilot start: back to normal")
    
    node = nodePrefix+pilotGenericName+".VSEL"
    await setBool( cpilot, node, 0 )
    node = nodePrefix+pilotGenericName+".ISEL"
    await setBool( cpilot, node, 0 )
    
    node = nodePrefix+pilotScadaName+".scadaGlobalMute"
    await setBool( cpilot, node, 0 )

    node = nodePrefix+pilotScadaName+".scadaCounterFlag"
    await setBool( cpilot, node, 0 )
    print("stage10_pilot end: back to normal OK")


# compare counters    
async def stage11_pilot( nodePrefix, cpilot, pilotGenericName, pilotScadaName ):
    print("stage11_pilot start: comparison")
    
    node = nodePrefix+pilotScadaName+".counters"
    counters = await get( cpilot, node)
    node = nodePrefix+pilotScadaName+".scadaGlobalCounter"
    scadaGlobalCounter = await get( cpilot, node)
    print("scadaGlobalCounter= ", scadaGlobalCounter)
    print("counters= ", counters)
    print("stage11_pilot end: comparison OK")

    
    

# ---start asyncio main, talking to two clients. 
# Do not use contxt manager, but explicit connect and disconnect
async def main():
    _logger.debug("===start test===")
    
    cpilot = Client(url=endpoint0)
    print("connecting cpilot")
    await cpilot.connect()
    ccaen = Client(url=endpoint1)
    print("connecting ccaen")
    await ccaen.connect()
    
    
    nsidx_caen = await ccaen.get_namespace_index( uri )
    nodePrefix_caen=f"ns={nsidx_caen};s="
    nsidx_pilot = await cpilot.get_namespace_index( uri )
    nodePrefix_pilot=f"ns={nsidx_pilot};s="

    generic0 = "genericPilot0"
    scada0 = "scadaCounters0"

    boardList=[0,3,5]
    
    # prepare caen and pilot with defaults
    await stage0_pilot( nodePrefix_pilot, cpilot, generic0, scada0)
    await stage0_caen( nodePrefix_caen, ccaen, "simSY4527", boardList)
    
    # mute counting, events, in pilot and caen. reset counters
    await stage1_2_pilot( nodePrefix_pilot, cpilot, generic0, scada0)

     # check that counters are zero
    await stage3_pilot( nodePrefix_pilot, cpilot, generic0, scada0)
    
    # start counting
    await stage4_pilot( nodePrefix_pilot, cpilot, generic0, scada0)
    
    # start ramp1
    await stage5_pilot( nodePrefix_pilot, cpilot, generic0, scada0)
    await stage5_caen( nodePrefix_caen, ccaen, "simSY4527", boardList)
    
    # reach plateau1
    await stage6_caen( nodePrefix_caen, ccaen, "simSY4527", boardList)
    
    # start ramp2
    await stage7_pilot( nodePrefix_pilot, cpilot, generic0, scada0)

    # reach plateau2
    await stage8_caen( nodePrefix_caen, ccaen, "simSY4527", boardList)
    
    # ramp down and check
    await stage9_caen( nodePrefix_caen, ccaen, "simSY4527", boardList)

    # back to normal
    await stage10_pilot( nodePrefix_pilot, cpilot, generic0, scada0)
    
    # comparison
    await stage11_pilot( nodePrefix_pilot, cpilot, generic0, scada0)

    await asyncio.sleep(1)
    print("disconnecting cpilot")
    await cpilot.disconnect()
    await asyncio.sleep(1)
    print("disconnecting ccaen")
    await ccaen.disconnect()
    _logger.debug("===end test===")

     # After one second we exit the Client context manager - this will close the connections also automatically
     # but we prefer to close the contaxt manually since we have 2 connections
    await asyncio.sleep(1)
    print("end")

if __name__ == '__main__':
    asyncio.run(main())
# ---end asyncio main---

