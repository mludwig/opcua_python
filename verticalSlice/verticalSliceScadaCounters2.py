# client to read/write to a venus pilot OPCUA server to perform
# the consistency test as specified in:
# https://readthedocs.web.cern.ch/display/ICKB/test_fwCaen_CaenEventModeCounter0
#
# talks to the opcua venus pilot server, which runs on a VM
# inject into docker gitlab-registry.cern.ch/mludwig/opcua_python:opcua-asyncio.cs8.1.0.0
#
# see api at:
# https://python-opcua.readthedocs.io/en/latest/client.html
# v0.9.92 https://github.com/FreeOpcUa/opcua-asyncio.git
import asyncio
import sys
import os
import time
import logging

sys.path.append("./")
sys.path.append("/usr/bin/python3")

from asyncua import Node, Client, ua
from datetime import datetime

# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
logging.basicConfig(level=logging.INFO)
#logging.basicConfig(level=logging.DEBUG)
_logger = logging.getLogger('asyncua')

# ---common---
# pilot
#endpoint0 = "opc.tcp://fdvs-venus-caen0:23400"
endpoint0 = "opc.tcp://localhost:23400"
# caen
# endpoint1 = "opc.tcp://fdvs-venus-caen0:4901"
endpoint1 = "opc.tcp://localhost:4901"
globalName = "opcuaserver" 
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
# ------------
 

# -- asyncio functions---

# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class SubscriptionHandler:
    """
    The SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        #_logger.info('datachange_notification %r %s', node, val)
        _logger.warning("new data arrived: %r= %s", node, val)



# read a scalar, list, anything
async def get( cl, varname ):
    var = cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value

# set a scalar Int32
async def setInt32( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Int32 )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a scalar Float
async def setFloat( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a scalar Boolean
async def setBool( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Boolean )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a python list as floats into AS
async def setListFloat( cl, parname, listToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", listToSet)
    parnode = cl.get_node( parname )
    #uav = ua.Variant( listToSet, ua.VariantType.Float, ua.ValueRank.OneDimension )
    uav = ua.Variant( listToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= ", uav)
    await parnode.set_value( uav )

# stage0: prepare the test
# full parameter call set for  completeness, always all channels 0...11
async def stage0_pilot( cpilot, pilotGenericName, pilotScadaName ):
    print("stage0_pilot start")
    
    nsidx0 = await cpilot.get_namespace_index( uri )
    nodePrefix0=f"ns={nsidx0};s="

    dictPilot = {
        "VSEL": nodePrefix0+pilotGenericName+".VSEL",
        "ISEL": nodePrefix0+pilotGenericName+".ISEL",
        "allChannelsLoad": nodePrefix0+pilotGenericName+".allChannelsLoad"
    }

    # pilot: unmute events to that the caen server stays connected
    node = nodePrefix0+pilotScadaName+".scadaGlobalMute"
    await setBool( cpilot, node, 0 )

    await asyncio.sleep(3)  # ofr the caen opc to pick up and reconnect

    # pilot: set noise, loads all channels
    await setBool( cpilot, dictPilot["VSEL"], 0 )
    await setBool( cpilot, dictPilot["ISEL"], 0 )
    await setListFloat( cpilot, dictPilot["allChannelsLoad"], [ 1e6, 1e-6 ] )
    

    print("stage0_pilot end")


async def stage0_caen( ccaen, crateName, boardList ):
    print("stage0_caen start: caen prepare settings")
    v0set = 19.8
    v1set = 200.0
    i0set = 0.1
    i1set = 1.0
    rup = 5.0
    rdown = 5.0
    
    nsidx0 = await ccaen.get_namespace_index( uri )
    nodePrefix0=f"ns={nsidx0};s="
    
    # prepare caen: all channels V0Set, V1Set, I0Set, I1Set
    for b in boardList:
        for c in range(12):
            bStr = f'{b:02d}'
            cStr = f'{c:03d}'
            node = nodePrefix0+crateName+".Board"+bStr+".Chan"+cStr+".Pw"
            await setInt32( ccaen, node, 0 )
            _logger.debug("stage0_caen: set Pw=0 for ", node)
            
            node = nodePrefix0+crateName+".Board"+bStr+".Chan"+cStr+".V0Set"
            await setFloat( ccaen, node, v0set )
            _logger.debug("stage0_caen: set V0Set= ", v0set," for ", node)

            node = nodePrefix0+crateName+".Board"+bStr+".Chan"+cStr+".V1Set"
            await setFloat( ccaen, node, v1set )
            _logger.debug("stage0_caen: set V1Set= ", v1set," for ", node)

            node = nodePrefix0+crateName+".Board"+bStr+".Chan"+cStr+".I0Set"
            await setFloat( ccaen, node, i0set )
            _logger.debug("stage0_caen: set I0Set= ", i0set," for ", node)

            node = nodePrefix0+crateName+".Board"+bStr+".Chan"+cStr+".I1Set"
            await setFloat( ccaen, node, i1set )
            _logger.debug("stage0_caen: set I1Set= ", i1set," for ", node)

            node = nodePrefix0+crateName+".Board"+bStr+".Chan"+cStr+".RUp"
            await setFloat( ccaen, node, rup )
            _logger.debug("stage0_caen: set rup= ", rup," for ", node)
            
            node = nodePrefix0+crateName+".Board"+bStr+".Chan"+cStr+".RDWn"
            await setFloat( ccaen, node, rdown )
            _logger.debug("stage0_caen: set rdown= ", rdown," for ", node)

    print("stage0_caen end: caen settings are prepared OK")

# mute and but stop counting
async def stage1_pilot( cpilot, pilotGenericName, pilotScadaName ):
    print("stage1_pilot start: stopping scada counters")
    nsidx0 = await cpilot.get_namespace_index( uri )
    nodePrefix0=f"ns={nsidx0};s="

    node = nodePrefix0+pilotScadaName+".scadaCounterFlag"
    await setBool( cpilot, node, 0 )
    
    node = nodePrefix0+pilotScadaName+".scadaGlobalMute"
    await setBool( cpilot, node, 1 )
    print("stage1_pilot wait 5 sec for crate to unsubscribe so that crate is assumed dead")
    await asyncio.sleep(5)

    print("stage1_pilot end: scada counters are stopped")

# reset global counters, write true, flag flips back to false.
# chec the results
async def stage2_3_pilot( cpilot, pilotGenericName, pilotScadaName ):
    print("stage2_3_pilot start: resetting scada counters")
    nsidx0 = await cpilot.get_namespace_index( uri )
    nodePrefix0=f"ns={nsidx0};s="
    node = nodePrefix0+pilotScadaName+".scadaGlobalReset"

    await setBool( cpilot, node, 1 )
    await asyncio.sleep(10)
    # read it back: it flips to false
    scadaGlobalReset = await get( cpilot, node )
    assert scadaGlobalReset == False, "scada counter reset did not work"
    
    await asyncio.sleep(10)
    node = nodePrefix0+pilotScadaName+".scadaGlobalCounter"
    globalCounter = await get( cpilot, node )
    print("stage2_3_pilot: globalCounter= ", globalCounter )
    assert globalCounter == 0, "globalCounter was not reset and shows not 0"
    
    node = nodePrefix0+pilotScadaName+".counters"
    counters = await get( cpilot, node )
    # that array should contain only 0 now
    # print("counters= ", counters)
    sum = 0
    for i in range( len(counters) ):
        sum += counters[i]
        
    assert sum == 0, "item counter (array) was not reset and shows not 0 everywhere"
  
    print("stage2_3_pilot end: scada counters are reset")



# unmute and start counting
async def stage4_pilot( cpilot, pilotGenericName, pilotScadaName ):
    print("stage4_pilot start: unmute and start counting")
    nsidx0 = await cpilot.get_namespace_index( uri )
    nodePrefix0=f"ns={nsidx0};s="


    node = nodePrefix0+pilotScadaName+".scadaCounterFlag"
    await setBool( cpilot, node, 1 )

    node = nodePrefix0+pilotScadaName+".scadaGlobalMute"
    await setBool( cpilot, node, 0 )
    print("stage4_pilot end: unmute and start counting OK")


# start ramping all channels by setting PW=ON
async def stage5_caen( ccaen, crateName, boardList ):
    print("stage5_caen start: caen start ramp Pw=true")
    
    nsidx0 = await ccaen.get_namespace_index( uri )
    nodePrefix0=f"ns={nsidx0};s="
    
    # prepare caen: all channels V0Set, V1Set, I0Set, I1Set
    for b in boardList:
        for c in range(12):
            bStr = f'{b:02d}'
            cStr = f'{c:03d}'
            node = nodePrefix0+crateName+".Board"+bStr+".Chan"+cStr+".Pw"
            await setInt32( ccaen, node, 1 )
            _logger.debug("stage0_caen: set Pw=1 for ", node)

    print("stage5_caen end: caen start ramp Pw=true OK")


# ---start asyncio main, talking to two clients. 
# Do not use contxt manager, but explicit connect and disconnect
async def main():
    _logger.debug("===start test===")
    
    generic0 = "genericPilot0"
    scada0 =  "scadaCounters0"
    
    cpilot = Client(url=endpoint0)
    print("connecting cpilot")
    await cpilot.connect()
    ccaen = Client(url=endpoint1)
    print("connecting ccaen")
    await ccaen.connect()
    
    # test I/O pilot
    root0 = cpilot.get_root_node()
    _logger.debug("root0 node= %r", root0)
    nsidx0 = await cpilot.get_namespace_index( uri )
    _logger.debug("namespace index0= %d", nsidx0)
    nodePrefix0=f"ns={nsidx0};s="
    _logger.debug("nodePrefix0= %s", nodePrefix0)
    i_slot = await get( cpilot, nodePrefix0+"genericPilot0.I_slot" )
    print("check: i_slot= ", i_slot)
    
    # test I/O caen
    root1 = ccaen.get_root_node()
    _logger.debug("root1 node= %r", root1)
    nsidx1 = await ccaen.get_namespace_index( uri )
    _logger.debug("namespace index1= %d", nsidx1)
    nodePrefix1=f"ns={nsidx1};s="
    _logger.debug("nodePrefix1= %s", nodePrefix1)
    Board00_Chan000_V0Set = await get( ccaen, nodePrefix1+"simSY4527.Board00.Chan000.V0Set" )
    print("check: Board00_Chan000_V0Set= ", Board00_Chan000_V0Set)



    boardList=[0,3,5]
    
    # prepare caen and pilot with defaults. events have to unmuted for that
    await stage0_pilot( cpilot, generic0, scada0 )
    await stage0_caen( ccaen, "simSY4527", boardList)
    
    # mute mute counting, events, in pilot and caen
    await stage1_pilot( cpilot, generic0, scada0 )

    # reset counters
    await stage2_3_pilot( cpilot, generic0, scada0 )
 
    # unmute (publish again) and start counting 
    await stage4_pilot( cpilot,  generic0, scada0 )

    await asyncio.sleep(1)
    print("disconnecting cpilot")
    await cpilot.disconnect()
    await asyncio.sleep(1)
    print("disconnecting ccaen")
    await ccaen.disconnect()
    _logger.debug("===end test===")

    
        
 

    #_logger.info("ramping down with parameters= %s %s", rampParams, " and finish" )
        

    # After one second we exit the Client context manager - this will close the connection.
    await asyncio.sleep(1)
    print("end")

if __name__ == '__main__':
    asyncio.run(main())
# ---end asyncio main---

