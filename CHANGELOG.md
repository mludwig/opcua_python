# changelog.md

### [ 15.jan.2025 ]
- added venus/venusOpcCaenRandomRamps12channels.py: this does ramps for one board but in a random way which NEVER occurs in operation but which is entirely within the specifications.


### [ 6.jan.2024 ]
- rebuilt and delivered image for alma9
- cleaned up obsolete files and OSes
- updated README.mdls
- added py scripts for venus testing in subdir
- added venus combo opcua caen simple ramp test, hardcoded on Board00.Chan000


## verticalSliceScadaCounters.py
### [ 13.may.2022 ]
- added aquisition of nbOfChannels for each board to avoid the hardcoded 12
- improves namespace handling, added delays


## vericalSliceBasic.py
- not beautiful but works

## opcua_asFormatDump.py
- needs a small review in the browsing recursive algorith since the node names have changed. Not a big deal, but check
- currently disfunctional (no output)
