# siemens s7 PLC test client
# OPCUA server for .open6 and .ua toolkit
# measure TSPP performance

import asyncio
import sys

sys.path.insert(0, "..")
import logging
from asyncua import Client, Node, ua
from datetime import datetime

import icsfd


# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
logging.basicConfig(level=logging.WARNING)
_logger = logging.getLogger( "s7test" )


# ---common---
endpoint0 = "opc.tcp://pcen33951.cern.ch:4841"
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
 

# subscription handler for AIR class objetcs
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py

# hook up to all objects
class Handler:
    count = 0  
    def datachange_notification(self, node: Node, val, data):
        Handler.count += 1

class CounterHandler:
    """
    count how many times the counter has reached 100 in order to determine
    the end of the test
    """
    t_current = datetime.now();
    t_previous = datetime.now();
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
  
        CounterHandler.t_previous = CounterHandler.t_current;
        CounterHandler.t_current = datetime.now();
        delay = CounterHandler.t_current - CounterHandler.t_previous
        _logger.warning("CounterHandler new data arrived: %r= %s delay= %d us", node, val, delay.microseconds)
  

async def makeTestNodeList( cl, prefix ):
    nlist=[]
    count = 0
    for i in range(1, 2001):
       name=prefix+"TEST3_AIR_"+str(i).zfill(4)+".PosSt.value"
       _logger.warning( name )
       node= cl.get_node( name )
       nlist.append( node )
       count += 1

    for i in range(1, 1001):
       name=prefix+"TEST3_AI_"+str(i).zfill(4)+".PosSt.value"
       _logger.warning( name )
       node= cl.get_node( name )
       nlist.append( node )
       count += 1

    for i in range(1, 1001):
       name=prefix+"TEST3_AOR_"+str(i).zfill(4)+".PosSt.value"
       _logger.warning( name )
       node= cl.get_node( name )
       nlist.append( node )
       count += 1

    for i in range(1, 1001):
       name=prefix+"TEST3_AO_"+str(i).zfill(4)+".PosSt.value"
       _logger.warning( name )
       node= cl.get_node( name )
       nlist.append( node )
       count += 1

    _logger.warning( "adding %d nodes to list", count )

    return nlist 




# set a value to the PLC using the enable and confirmation read back. Need the 3 adddresses accordingly
async def plcSet( cl, varname, enable, confirm, value ):
    var=cl.get_node( varname )
    ena=cl.get_node( enable )

    await icsfd.set( cl, varname, value )
    _logger.debug("set value for [%s %s %s]= %r", varname, enable, confirm, value)
    await icsfd.set( cl, enable, 64 )
    _logger.debug("set enable for [%s %s %s]= %r", varname, enable, confirm, value)
    conf = await icsfd.get( cl, confirm )
    _logger.debug("set waiting for confirmation for [%s %s %s]= %r", varname, enable, confirm, value)
    while ( conf != value ):
       await asyncio.sleep(0.1)	# takes a couple of turns on the PLC
       conf = await icsfd.get( cl, confirm )
       #_logger.warning("waiting for confirmation [%s %s %s]= %r", varname, enable, confirm, value)

    if  conf == value:
       await icsfd.set( cl, enable, 0 )
       _logger.debug("set confirmed for [%s %s %s]= %r", varname, enable, confirm, value)
    else:
       _logger.error("failed to set [%s %s %s]= %r", varname, enable, confirm, value)
       sys.exit( "failed confirmation on PLC set")


# ---start asyncio main---
async def main():
    _logger.info("===start init===")
    async with Client(url=endpoint0) as client:
        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        _logger.debug("root node= %r", root)
        nsidx = await client.get_namespace_index( uri )
        _logger.debug("namespace index= %d", nsidx)
        nodePrefix=f"ns={nsidx};s="
        _logger.info("nodePrefix= %s", nodePrefix)
        _logger.info("===end init===")

       
        # set the test parameters according to https://readthedocs.web.cern.ch/pages/viewpage.action?spaceKey=ICKB&title=S7-300-400+OPCUA+TSPP+simple+test
 

        # data volume in units of 100 addresses
        nbBunches_node = nodePrefix+"TEST3_BunchesOfIO_APAR.MPosR.value"
        nbBunches_enable = nodePrefix+"TEST3_BunchesOfIO_APAR.ManReg01.value"
        nbBunches_confirm = nodePrefix+"TEST3_BunchesOfIO_APAR.MPosRSt.value"
        nbBunches_value = 100000
        await plcSet( client, nbBunches_node, nbBunches_enable, nbBunches_confirm, nbBunches_value )
        _logger.info( "%s set to %f", nbBunches_node, nbBunches_value )
 
        # looping test repeats
        nbCycles_node = nodePrefix+"TEST3_NbOfCycles_APAR.MPosR.value"
        nbCycles_enable = nodePrefix+"TEST3_NbOfCycles_APAR.ManReg01.value"
        nbCycles_confirm = nodePrefix+"TEST3_NbOfCycles_APAR.MPosRSt.value"
        nbCycles_value = 1000
        await plcSet( client, nbCycles_node, nbCycles_enable, nbCycles_confirm, nbCycles_value )
        _logger.info( "%s set to %f", nbCycles_node, nbCycles_value )


        # PLC sampling time in ms. That is the PLC loop time. "If we go too fast we loose values"
        # whatever this means
        samplingTime_node = nodePrefix+"TEST3_SamplingTime_APAR.MPosR.value"
        samplingTime_enable = nodePrefix+"TEST3_SamplingTime_APAR.ManReg01.value"
        samplingTime_confirm = nodePrefix+"TEST3_SamplingTime_APAR.MPosRSt.value"
        samplingTime_value = 400
        await plcSet( client, samplingTime_node, samplingTime_enable, samplingTime_confirm, samplingTime_value )
        _logger.info( "%s set to %f ms", samplingTime_node, samplingTime_value )



        # hook up the counter to know when the test is actually finished
        counter_node = nodePrefix+"TEST3_Counter_AS.PosSt.value"
        node0= client.get_node( counter_node )
        counter_handler = CounterHandler()
        timeoutInSecs=10
        counter_subscription = await client.create_subscription( timeoutInSecs, counter_handler )
        await counter_subscription.subscribe_data_change( node0 )
 
   
 
        # hook up the subscription handlers to get some test results.
        # lets hook up one handler to all objetcs and just count how many times
        # objetcs (=addresses) received a change
        # https://readthedocs.web.cern.ch/display/ICKB/Siemens+S7+OPCUA+Server+Performance+Tests
        nodeList = await makeTestNodeList( client, nodePrefix )        
        #_logger.warning( "nodeList= %s", nodeList )
        xhandler = Handler()
        timeoutInSecs=100
        handler_subscription = await client.create_subscription( timeoutInSecs, xhandler )

        # subscribe
        await handler_subscription.subscribe_data_change( nodeList )


        # start the test 
        start_node = nodePrefix+"TEST3_StartTest_DPAR.ManReg01.value"
        await icsfd.set( client, start_node, 16 )
        await asyncio.sleep(2)
        await icsfd.set( client, start_node, 0 )


        # what are the test end conditions ?
        # TEST3_COUNTER_ASPosStr.value goes from 0->100 nbCycle times, but between each runs it goes down to 0
        # so let's assume that if we read it 3 times consecutively with 2 secs in between at 0 the test 
        # is finished. But wait for the test to start properly before.
        counter_value = await icsfd.get( client, counter_node )

        endCondition = 3

        ## switch off for debugging
        #counter_value = 0
        #endCondition = 0
        while counter_value > 0 or endCondition > 0 :
           await asyncio.sleep(2)	
           counter_value = await icsfd.get( client, counter_node )
           _logger.warning( "checking end condition: counter_value= %f endCondition= %f", counter_value, endCondition )
           if counter_value == 0.0:
              endCondition -= 1
 
        _logger.warning( "test finished, reading out results" )
 
        # results
        _logger.warning( "Handler.count= %f", Handler.count )


        # somewhat optionally
        await counter_subscription.delete()
        await handler_subscription.delete()




if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.run_until_complete(main())
    loop.close()

# ---end asyncio main---

