# demo client to read/write to an OPCUA server for .open6 toolkit


# ---start asycio---
import asyncio
import sys

sys.path.insert(0, "..")
import logging
from asyncua import Client, Node, ua
from datetime import datetime

# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
logging.basicConfig(level=logging.INFO)

_logger = logging.getLogger('asyncua')

# ---end asyncio

# ---common---
endpoint0 = "opc.tcp://localhost:4841"
globalName = "opcuaserver" 
#globalUrn = "urn:JCOP:OpcUaCaenServer"
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
#nodePrefix='ns=2;s='
 

# -- start asyncio functions---


# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class SubscriptionHandler:
    """
    The SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        #_logger.info('datachange_notification %r %s', node, val)
        _logger.warning("new data arrived: %r= %s", node, val)



# get value from a var name like 'TEST3_CycleTime_AS.PosSt.value'
async def get( cl, varname ):
    var=cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value


# read to get the variant type and then set
async def set( cl, parname, valueToSet ):
    _logger.debug("set-get %s", parname)
    par=cl.get_node( parname )

    dvx=await par.get_data_value()    # need the type of the variant
    dv = ua.DataValue(ua.Variant( valueToSet, dvx.Value.VariantType))
    dv.ServerTimestamp = None         # needed, otherwise .ua will not accept
    dv.SourceTimestamp = None
    await par.set_value(dv)
    _logger.debug("set %s to %s", parname, valueToSet)


# ---start asyncio main---
async def main():
    _logger.info("===start init===")
    async with Client(url=endpoint0) as client:
        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        _logger.debug("root node= %r", root)
        nsidx = await client.get_namespace_index( uri )
        _logger.debug("namespace index= %d", nsidx)
        nodePrefix=f"ns={nsidx};s="
        _logger.info("nodePrefix= %s", nodePrefix)
        _logger.info("===end init===")


        # get a var as fast as possible
        for i in range(1,10):
           var0=nodePrefix+"TEST3_CycleTime_AS.PosSt.value"
           val0 = await get( client, var0 )
           _logger.warning("GET %s= %r", var0, val0 )


        # set a var as fast as possible to different values. Type recognition is automatic
        # depending how fast the OPCUA server's AS is updated you might not see a lot actually
        for i in range(1,10):
           par1=nodePrefix+"TEST3_BunchesOfIO_APAR.MPosR.value"
           val1=16+i
           await set( client, par1, val1 )
           _logger.warning("SET %s= %r", par1, val1 )
           await asyncio.sleep( 3 )

    
        # subscription
        #
        # this is an example how to go down the tree to find the node "WATCHDOG.counter.value"
        # see here for more examples and good stuff
        # https://github.com/FreeOpcUa/python-opcua/blob/master/opcua/common/node.py
        n_root=client.get_root_node()
        _logger.info("n_root= %s", n_root )
        n_objects=client.get_objects_node()
        _logger.info("n_objects= %s", n_objects )
        n_watchdog= await n_objects.get_child([f"{nsidx}:WATCHDOG"])
        _logger.info("n_watchdog= %s", n_watchdog )
        n_counter= await n_watchdog.get_child(f"{nsidx}:counter")
        _logger.info("n_counter= %s", n_counter )
        n_value= await n_counter.get_child(f"{nsidx}:value")
        _logger.info("n_value= %s", n_value )
        n_address= await n_counter.get_child(f"{nsidx}:address")
        _logger.info("n_address= %s", n_address )
 
        # better do this in one go if you know the node you want
        nn_name=nodePrefix+"WATCHDOG.counter.value"
        nn_node= client.get_node( nn_name )
        handler = SubscriptionHandler()
        timeoutInSecs=500
        subscription = await client.create_subscription( timeoutInSecs, handler )

        # we can subscribe to several nodes with the same handler
        #nodes = [ n_value ] # or: from above example, whatever
        nodes = [ nn_node ]

        # subscribe
        await subscription.subscribe_data_change( nodes )
     
        # wait for a few value hanges to arrive
        await asyncio.sleep(30)

        # We delete the subscription (this un-subscribes from the data changes of the two variables).
        # This is optional since closing the connection will also delete all subscriptions.
        await subscription.delete()

        # After one second we exit the Client context manager - this will close the connection.
        await asyncio.sleep(1)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.run_until_complete(main())
    loop.close()

# ---end asyncio main---

