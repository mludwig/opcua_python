# package to simplyfy asyncio for OPCUA servers

import asyncio
import sys

sys.path.insert(0, "..")
import logging
from asyncua import Client, Node, ua
from datetime import datetime

#logging.basicConfig(level=logging.WARNING)
_logger = logging.getLogger("icsfd")


# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class DemoSubscriptionHandler:
    """
    The Demo SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        # set the logger to DEBUG in order to see this
        _logger.debug("new data arrived: %r= %s", node, val)



# get value from a var name like 'TEST3_CycleTime_AS.PosSt.value'
async def get( cl, varname ):
    var=cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value


# read to get the variant type and then set
async def set( cl, parname, valueToSet ):
    _logger.debug("set-get %s", parname)
    par=cl.get_node( parname )

    dvx=await par.get_data_value()    # need the type of the variant
    dv = ua.DataValue(ua.Variant( valueToSet, dvx.Value.VariantType))
    dv.ServerTimestamp = None         # needed, otherwise .ua will not accept
    dv.SourceTimestamp = None
    await par.set_value(dv)
    _logger.debug("set %s to %s", parname, valueToSet)


