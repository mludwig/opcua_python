#!/bin/bash
# inject a opcua python script into a container runing on an openstack vm.
# the container inside the vm mounts the directory /vm-mount to /vm-mount, in a bidirectional mode.
# This mounting is done when the container is launched, inside the init-vm.sh which creates the /venus-caen-combo-start.sh
# Then, once the container runs, it looks into the /vm-mount directory for the file sim_config.xml (as a supervisor task)
# if it finds /vm-mount/client.py inside the container, it executes it once (wait until it is finished..) 
#
# therefore, a new script only needs to be copied into the openstack vm as /vm-mount/client.py
if [[ $1 == "" && $2 == "" ]]; then
	echo "inject a new python script into a opcua-python vm: input is missing"
	echo $0" <opcua-python-hostname> <newscript.py>"
fi

# $1=hostname
vmhost=$1.cern.ch
if [[ $vmhost == "" ]]; then
	read -p "(openstack-vm) hostname (no cern.ch required): " userin1
	vmhost=$userin1".cern.ch"
fi
# $2=new py script
newscript=$2
if [[ $newscript == "" ]]; then
	read -p "py script : " userin2
	newscript=$userin2
fi
echo "opcua-python: injecting new script "$newscript" into opcua-python VM in "$vmhost
#scp -i ./opcua-python.pem ${newscript} root@${vmhost}:/vm-mount/client.py
scp -i ./venus-cal9.rsa.private ${newscript} root@${vmhost}:/vm-mount/client.py
