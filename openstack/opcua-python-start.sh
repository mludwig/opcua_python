#!/bin/bash
# start the venus combo on a alma9 openstack VM
# project access token for qaToolbox/venus-caen images: 
# token: openstack-reporter-api: api, role: reporter
token="glpat-3r_Sx9NDoiYG9D-B4TsL"
tokenrole="reporter"
dnf update -y
dnf install -y podman
# ---------------------------
# open ports through firewall
# ---------------------------
# opc/glue to endpoint:
firewall-cmd --zone=public --permanent --add-port=4841/tcp
# venus pilot to endpoint:
firewall-cmd --zone=public --permanent --add-port=23400/tcp
firewall-cmd --reload
#
podman system prune --all -f
registry=gitlab-registry.cern.ch/mludwig
image=$registry/opcua_python:opcua-asyncio.cal9
echo "image= "$image
podman login -u $tokenrole -p $token $registry
podman run --net=host --expose=4841 --expose=4901 --expose=8890 gitlab-registry.cern.ch/mludwig/opcua_python:opcua-asyncio.cal9


