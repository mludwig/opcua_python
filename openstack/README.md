# openstack readme
## how to deploy a opcua-python image on an openstack VM

- create a VM on https://openstack.cern.ch/project "BE caensim" or elsewhere :
- name: opcuapython-XXX
- variant: ALMA9-x86_64
- memory: m2.large
- key: opcua-python
- load customization script: ./init-vm.sh
- the container is started with the container running as a service.
- the bidirectional mount is at /vm-mount/client.py, inject your scripts there 


## authentication
- from lxplus: ssh root@opcuapython-XXX  (this will try all keys in your ~/.ssh, which is afs.cern.ch/... usually )

### from inside CERN
- download the private key opcua-python.pem into your ~/.ssh to your host (or use an editor copy/paste)
- chmod 0644 ~/.ssh/opcua-python.pem
- ssh -i ~/.ssh/opcua-python opcuapython-XXX.cern.ch




