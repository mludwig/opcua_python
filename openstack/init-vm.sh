#!/bin/bash
# create the opcua-python system service
# copy this at VM creation so that it is executed at VM init
#
# create the opcua-python service startup script on the fly
opcua_python_start_script=/opcua-python-start.sh
echo "#!/bin/bash" > ${opcua_python_start_script}
echo "dnf update -y" >> ${opcua_python_start_script}
echo "dnf install -y podman" >> ${opcua_python_start_script}
# see https://confluence.cern.ch/display/ICKB/JCOP+Server+Port+Allocations for port allocations
echo "firewall-cmd --zone=public --permanent --add-port=4841/tcp" >> ${opcua_python_start_script}
echo "firewall-cmd --zone=public --permanent --add-port=4891/tcp" >> ${opcua_python_start_script}
echo "firewall-cmd --zone=public --permanent --add-port=4901/tcp" >> ${opcua_python_start_script}
echo "firewall-cmd --zone=public --permanent --add-port=8880/tcp" >> ${opcua_python_start_script}
echo "firewall-cmd --zone=public --permanent --add-port=8881/tcp" >> ${opcua_python_start_script}
echo "firewall-cmd --zone=public --permanent --add-port=23400/tcp" >> ${opcua_python_start_script}
echo "firewall-cmd --zone=public --permanent --add-port=23500/tcp" >> ${opcua_python_start_script}
echo "firewall-cmd --zone=public --permanent --add-port=23600/tcp" >> ${opcua_python_start_script}
echo "firewall-cmd --zone=public --permanent --add-port=24911/tcp" >> ${opcua_python_start_script}
echo "firewall-cmd --reload" >> ${opcua_python_start_script}
echo "mkdir /vm-mount"  >> ${opcua_python_start_script}
echo "podman system prune --all -f" >> ${opcua_python_start_script}
# opcua-python-api-reporter.gitlab-token.txt
echo "podman login -u \"reporter\" -p \"glpat-zujR-BNaLD7L2tc41jsP\" gitlab-registry.cern.ch/mludwig" >> ${opcua_python_start_script}
echo "podman run --net=host --expose=4841 --expose=4891 --expose=4901 --expose=8880 --expose=8881 --expose=23400 --expose=23500 --expose=23600 --expose=24911 --volume /vm-mount:/vm-mount:rw,Z gitlab-registry.cern.ch/mludwig/opcua_python:opcua-asyncio.cal9"  >> ${opcua_python_start_script}
#
chmod +x ${opcua_python_start_script}
#
# create the systemd service file on the fly, using the start_script /opcua-python-start.sh from above
opcua_python_service_file=/usr/lib/systemd/system/opcua-python.service
opcua_python_service_link=/etc/systemd/system/multi-user.target.wants/opcua-python.service
echo "[Unit]" > ${opcua_python_service_file}
echo "Description=OPCUA python interface for injected script execution" >> ${opcua_python_service_file}
echo "After=network.target" >> ${opcua_python_service_file}
echo "" >> ${opcua_python_service_file}
echo "[Service]" >> ${opcua_python_service_file}
echo "ExecStart=/bin/bash /opcua-python-start.sh" >> ${opcua_python_service_file}
echo "" >> ${opcua_python_service_file}
echo "[Install]" >> ${opcua_python_service_file}
echo "WantedBy=multi-user.target" >> ${opcua_python_service_file}
# 
# start the service. It will come up after reboots as well
chmod 644 ${opcua_python_service_file}
ln -s ${opcua_python_service_file} ${opcua_python_service_link} 
systemctl daemon-reload
systemctl enable opcua-python.service
systemctl start opcua-python.service
#check
systemctl status opcua-python.service
# check
#systemctl list-units --type service | grep opcua-python
#
