# demo client to read/write to an OPCUA server to perform 
# a consistency and performance test:
# ramp up and down all channels, but with different target voltage each. 
# do this repetitive 
#
#
# talks only the opcua venus OPCUA Caen server, which runs in a venus combo
#
# see api at:
# https://python-opcua.readthedocs.io/en/latest/client.html
# v0.9.92 https://github.com/FreeOpcUa/opcua-asyncio.git
import asyncio
import sys
import os
import time
import logging
import random
import time

sys.path.append("./")
sys.path.append("/usr/bin/python3")

from asyncua import Node, Client, ua
from datetime import datetime

# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('asyncua')

# ---common---
# use a local endpoint 
endpoint0 = "opc.tcp://localhost:4841"
#endpoint0 = "opc.tcp://combotest-1.cern.ch:4841"
globalName = "opcuaserver" 
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
# ------------
 

# -- asyncio functions---

# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class SubscriptionHandler:
    """
    The SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        #_logger.info('datachange_notification %r %s', node, val)
        _logger.warning("new data arrived: %r= %s", node, val)



# read a scalar, list, anything
async def get( cl, varname ):
    var = cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value

# set a scalar Int32
async def setInt32( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Int32 )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a scalar Float
async def setFloat( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)


# set a scalar Boolean
async def setBool( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Boolean )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a python list as floats into AS
async def setListFloat( cl, parname, listToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", listToSet)
    parnode = cl.get_node( parname )
    #uav = ua.Variant( listToSet, ua.VariantType.Float, ua.ValueRank.OneDimension )
    uav = ua.Variant( listToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= ", uav)
    await parnode.set_value( uav )


# ---start asyncio main---
async def main():
    _logger.info("===start init===")
    async with Client(url=endpoint0) as client:
        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        _logger.debug("root node= %r", root)
        nsidx = await client.get_namespace_index( uri )
        _logger.debug("namespace index= %d", nsidx)
        nodePrefix=f"ns={nsidx};s="
        _logger.info("nodePrefix= %s", nodePrefix)
        _logger.info("===end init===")
        random.seed( datetime.now().timestamp())
        
        # define a dictionary for the OPCUA Address Space
        # we want to set params for a ramp and start the ramp on bord0.channel0
        
        crate = "sim4527"
        
        aschannel_array = [
            "V0Set",
            "V1Set",
            "I0Set",
            "I1Set",
            "RUp",
            "RDWn",
            "SVMax",
            "Pw"
            ]
                 
        # boards
        boards_array = ["Board00"]
            
        # the channels 
        channels12_array = [ "Chan000", "Chan001", "Chan002",
                             "Chan003", "Chan004", "Chan005",
                             "Chan006", "Chan007", "Chan008",
                             "Chan009", "Chan010" , "Chan011"]
        
        # set directly into OPC
        I0Set_value = 10
        I1Set_value = 15
        SVMax_value = 1000
        
        # ramp up. we assume all channels are off and show 0
        for board in boards_array:
            for channel in channels12_array:
                
                # construct AS dict on the fly, do not save anything
                for api in aschannel_array:
                    api_element = nodePrefix+crate+"."+board+"."+channel+"."+api
                    _logger.info("ramp up api= %s api_element= %s", api, api_element)
        
                    # need Rup, RDwn and V0Set, V1Set, randomized numerical values
                    RUp_random = int(1 + random.random() * 5)
                    RDwn_random = int(1 + random.random() * 5)
                    V0Set_random = 20 + random.random() * 200   
                    V1Set_random = 40 + random.random() * 500
                    
                    # relate the type of the api element to the value
                    # match - case for python >= 3.10 and we are 3.9.21
                    if api == "V0Set":
                        await setFloat( client, api_element, V0Set_random )
                    elif api == "V1Set": 
                        await setFloat( client, api_element, V1Set_random )
                    elif api == "RUp": 
                        await setFloat( client, api_element, RUp_random )
                    elif api == "RDWn": 
                        await setFloat( client, api_element, RDwn_random )
                    elif api == "I0Set": 
                        await setInt32( client, api_element, I0Set_value )
                    elif api == "I1Set": 
                        await setInt32( client, api_element, I1Set_value )
                    elif api == "SVMax": 
                        await setFloat( client, api_element, SVMax_value )    
                    elif api == "Pw": 
                        await setBool( client, api_element, True )    
                    else:
                        _logger.info("api %s not found/programmed, skip", api)
 
        # wait a bit
        _logger.info("waiting a bit for the ramps to start")
        time.sleep(2)
 
        # read the VMon to see if we have reached the end of the ramp
        countdown = 1000
        while countdown > 0:
            for board in boards_array:
                _logger.info("countdown= %d", countdown )
                for channel in channels12_array:
                    
                    # read VMon and compare it to V0Set
                    api_vmon = nodePrefix+crate+"."+board+"."+channel+".VMon"
                    vmon = await get( client, api_vmon )
                    #_logger.info("check plateau api_vmon= %s value= %f", api_vmon, vmon)
                         
                    api_v0set = nodePrefix+crate+"."+board+"."+channel+".V0Set"
                    v0set = await get( client, api_v0set )
                    #_logger.info("check plateau api_v0set= %s value= %f", api_v0set, v0set)
                    _logger.info("check plateau api_v0set= %s value= %f vmon= %f", api_v0set, v0set, vmon)
                           
                     
                    api_pw = nodePrefix+crate+"."+board+"."+channel+".Pw"
                    pw = await get( client, api_pw )
                    
                    # TOP reached
                    if (pw == 1) and (vmon * 1.05 > v0set) :
                        _logger.info("ramp TOP reached %s to 0", api_pw )
                        await setBool( client, api_pw, False )

                    # BOTTOM reached
                    if (pw == 0) and (vmon < 1.0 ) :
                        _logger.info("ramp BOTTOM reached %s to 1", api_pw )
                        await setBool( client, api_pw, True )

                            
            countdown -= 1
            time.sleep(1)
               
        print("end")

if __name__ == '__main__':
    asyncio.run(main())
# ---end asyncio main---

