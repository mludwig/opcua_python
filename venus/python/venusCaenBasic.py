# demo client to read/write to an OPCUA server to perform venus basic tests
#
# talks to the opcua venus OPCUA Caen server, which runs in a venus combo

#
# see api at:
# https://python-opcua.readthedocs.io/en/latest/client.html
# v0.9.92 https://github.com/FreeOpcUa/opcua-asyncio.git
import asyncio
import sys
import os
import time
import logging

sys.path.append("./")
sys.path.append("/usr/bin/python3")

from asyncua import Node, Client, ua
from datetime import datetime

# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('asyncua')

# ---common---
# use a local endpoint 
endpoint0 = "opc.tcp://localhost:4841"
globalName = "opcuaserver" 
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
# ------------
 

# -- asyncio functions---

# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class SubscriptionHandler:
    """
    The SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        #_logger.info('datachange_notification %r %s', node, val)
        _logger.warning("new data arrived: %r= %s", node, val)



# read a scalar, list, anything
async def get( cl, varname ):
    var = cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value

# set a scalar Int32
async def setInt32( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Int32 )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a scalar Float
async def setFloat( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)


# set a scalar Boolean
async def setBool( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Boolean )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a python list as floats into AS
async def setListFloat( cl, parname, listToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", listToSet)
    parnode = cl.get_node( parname )
    #uav = ua.Variant( listToSet, ua.VariantType.Float, ua.ValueRank.OneDimension )
    uav = ua.Variant( listToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= ", uav)
    await parnode.set_value( uav )


# ---start asyncio main---
async def main():
    _logger.info("===start init===")
    async with Client(url=endpoint0) as client:
        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        _logger.debug("root node= %r", root)
        nsidx = await client.get_namespace_index( uri )
        _logger.debug("namespace index= %d", nsidx)
        nodePrefix=f"ns={nsidx};s="
        _logger.info("nodePrefix= %s", nodePrefix)
        _logger.info("===end init===")

        # define a dictionary for the OPCUA Address Space
        # we want to set params for a ramp and start the ramp on bord0.channel0
        asdict = {
            "I0Set": nodePrefix+"sim4527.Board00.Chan000.I0Set",
            "I1Set": nodePrefix+"sim4527.Board00.Chan000.I1Set",
            "RUp": nodePrefix+"sim4527.Board00.Chan000.RUp",
            "RDWn": nodePrefix+"sim4527.Board00.Chan000.RDWn",
            "SVMax": nodePrefix+"sim4527.Board00.Chan000.SVMax",
            "V0Set": nodePrefix+"sim4527.Board00.Chan000.V0Set",
            "V1Set": nodePrefix+"sim4527.Board00.Chan000.V1Set",
            "Pw": nodePrefix+"sim4527.Board00.Chan000.Pw"
            }
        
        # set directly into OPC
        _logger.info("- setting parameters Board00.Chan000")
        _logger.info("setting value to api= %s",asdict["I0Set"] )
        await setInt32( client, asdict["I0Set"], 5 )
        
        await setInt32( client, asdict["I1Set"], 5 )
        await setInt32( client, asdict["RUp"], 5 )
        await setInt32( client, asdict["RDWn"], 5 )
        await setFloat( client, asdict["V0Set"], 100.0 )
        await setFloat( client, asdict["V1Set"], 200.0 )
        await setFloat( client, asdict["SVMax"], 1000.0 )
        
        # ramp up on V0/I0 first plateau with VSEL=0 and ISEL=0
        _logger.info("- ramp up")
        await setInt32( client, asdict["Pw"], 1 )
        
        _logger.info("- wait 20 sec")
        await asyncio.sleep(20)
        
        # ramp down
        _logger.info("- ramp down, and finish (not waiting)")
        await setInt32( client, asdict["Pw"], 0 )
        
        print("end")

if __name__ == '__main__':
    asyncio.run(main())
# ---end asyncio main---

