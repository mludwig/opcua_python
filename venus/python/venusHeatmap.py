# demo client to read vmon on many channels and produce a heatmap
#
# talks only the opcua venus OPCUA Caen server, which runs in a venus combo
#
# see api at:
# https://python-opcua.readthedocs.io/en/latest/client.html
# v0.9.92 https://github.com/FreeOpcUa/opcua-asyncio.git
import asyncio
import sys
import os
import time
import logging
import random
import time

import matplotlib.pyplot as plt
#import matplotlib as mpl
import numpy as np

sys.path.append("./")
sys.path.append("/usr/bin/python3")

from asyncua import Node, Client, ua
from datetime import datetime

# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('asyncua')

# ---common---
# use a local endpoint 
crate = "demo3"                  
endpoint0 = "opc.tcp://venuscaencombo-3.cern.ch:4841"
#endpoint0 = "opc.tcp://combotest-1.cern.ch:4841"
globalName = "opcuaserver" 
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
# ------------
 

# -- asyncio functions---

# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class SubscriptionHandler:
    """
    The SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        #_logger.info('datachange_notification %r %s', node, val)
        _logger.warning("new data arrived: %r= %s", node, val)



# read a scalar, list, anything
async def get( cl, varname ):
    var = cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value

# set a scalar Int32
async def setInt32( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Int32 )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a scalar Float
async def setFloat( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)


# set a scalar Boolean
async def setBool( cl, parname, valueToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", valueToSet)
    parnode = cl.get_node( parname )
    #print("parnode= ", parnode, " browse name= ", await parnode.read_browse_name())

    uav = ua.Variant( valueToSet, ua.VariantType.Boolean )
    _logger.debug( "set: uav= %s", uav)
    dv = ua.DataValue( uav )
    _logger.debug("set: dv= %s", dv)
    await parnode.set_value( dv )
    _logger.debug("set: %s to %s", parname, valueToSet)

# set a python list as floats into AS
async def setListFloat( cl, parname, listToSet ):
    _logger.debug("set: %s %s %s", parname, " to value= ", listToSet)
    parnode = cl.get_node( parname )
    #uav = ua.Variant( listToSet, ua.VariantType.Float, ua.ValueRank.OneDimension )
    uav = ua.Variant( listToSet, ua.VariantType.Float )
    _logger.debug( "set: uav= ", uav)
    await parnode.set_value( uav )

# show a 2d heatmap, see https://stackoverflow.com/questions/33282368/plotting-a-2d-heatmap
def heatmap2d(arr: np.ndarray):
    #_logger.info(plt.colormaps())
    #plt.figure(figsize = (20,2))
    #plt.imshow(arr, cmap='viridis')
    plt.imshow(arr, cmap='inferno')
    #plt.imshow(arr, cmap='inferno', extent=(0, 2000, 0, 400))
    plt.colorbar()
    plt.show(block=False)
    plt.pause(1)
    plt.close( 'all' )
    

# ---start asyncio main---
async def main():
    _logger.info("===start init===")
    async with Client(url=endpoint0) as client:
        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        _logger.debug("root node= %r", root)
        nsidx = await client.get_namespace_index( uri )
        _logger.debug("namespace index= %d", nsidx)
        nodePrefix=f"ns={nsidx};s="
        _logger.info("nodePrefix= %s", nodePrefix)
        _logger.info("===end init===")
        random.seed( datetime.now().timestamp())
        
        # define a dictionary for the OPCUA Address Space
        # we want to set params for a ramp and start the ramp on bord0.channel0
        # boards
        # printf '"Board%s"\n' {1..15} | paste -sd, - | sed 's/^/[/; s/$/]/'
        boards_array = [
            "Board00", "Board01", "Board02", "Board03", "Board04", "Board05", "Board06", "Board07", "Board08", "Board09",
"Board10", "Board11", "Board12", "Board13", "Board14", "Board15", "Board16", "Board17", "Board18", "Board19",
"Board20", "Board21", "Board22", "Board23", "Board24", "Board25", "Board26", "Board27", "Board28", "Board29",
"Board30", "Board31", "Board32", "Board33", "Board34", "Board35", "Board36", "Board37", "Board38", "Board39",
"Board40", "Board41", "Board42", "Board43", "Board44", "Board45", "Board46", "Board47", "Board48", "Board49",
"Board50", "Board51", "Board52", "Board53", "Board54", "Board55", "Board56", "Board57", "Board58", "Board59",
"Board60", "Board61", "Board62", "Board63", "Board64", "Board65", "Board66", "Board67", "Board68", "Board69",
"Board70", "Board71", "Board72", "Board73", "Board74", "Board75", "Board76", "Board77", "Board78", "Board79",
"Board80", "Board81", "Board82", "Board83", "Board84", "Board85", "Board86", "Board87", "Board88", "Board89",
"Board90", "Board91", "Board92", "Board93", "Board94", "Board95", "Board96", "Board97", "Board98", "Board99"                        
                        ]
        # the channels, same for al boards 
        channels12_array = [ "Chan000", "Chan001", "Chan002",
                             "Chan003", "Chan004", "Chan005",
                             "Chan006", "Chan007", "Chan008",
                             "Chan009", "Chan010" , "Chan011"]
        
  
        # initialize a heatmap: for each channel we detect the rate of change between the last two frames 
        # mpl.use('Agg')
        nbChannels = len(boards_array) * len(channels12_array) 
        nbFrames = 400
        heatmap_array = np.arange(nbChannels * nbFrames) 
        heatmap_array = heatmap_array.reshape(nbFrames, nbChannels)        
        #heatmap2d(heatmap_array)
        for f in range(0, nbFrames ):
            for c in range(0, nbChannels ):
                heatmap_array[ f, c ] = 0.0
        heatmap2d(heatmap_array)
 
    
        # read the VMon 
        countdown = 1000
        indexFrame = nbFrames - 1
        while countdown > 0:
            ich = 0
            _logger.info("countdown= %d", countdown )
            for board in boards_array:
                for channel in channels12_array:
                    
                    # read VMon and compare it to V0Set
                    api_vmon = nodePrefix+crate+"."+board+"."+channel+".VMon"
                    vmon = await get( client, api_vmon )
                    #_logger.info("api_vmon= %s value= %f channel=%d", api_vmon, vmon, ich)
                         
                    #api_v0set = nodePrefix+crate+"."+board+"."+channel+".V0Set"
                    #v0set = await get( client, api_v0set )                         
                                            
                    # update the heatmap: we just append each channel diff
                    # heatmap_array[indexFrame, ich]= v0set - vmon
                    heatmap_array[indexFrame, ich]= vmon
                    ich += 1

                
            # shift frames up: 0 is oldest and gets lost. we leave indexFrame at the end of the buffer
            # youngest frame is at 0 at the bottom
            for f in range(1, nbFrames ):
                for c in range(0, nbChannels ):
                    heatmap_array[ f-1, c ] = heatmap_array[ f, c ]
            
            countdown -= 1
            
            # we show the heatmap in its present state
            #heatmap_array = heatmap_array.reshape(nbFrames, nbChannels) 
                   
            heatmap2d(heatmap_array)
  
            time.sleep(3)          
               
        print("end")

if __name__ == '__main__':
    asyncio.run(main())
# ---end asyncio main---

