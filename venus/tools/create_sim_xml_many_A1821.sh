#!/bin/bash
# crate a sim_config.xml with **many* A1821 boards
fout="sim_config_many_A1821.xml"

cat "./header_sim_config.txt" > $fout
#for iboard in {1..100}
for iboard in $(seq -f "%02g" 0 99)
do
   # add   <Board name="Board00" slot="0" type="A1821" description="12 Channel 3 kV, 200/20 uA"></Board>
    echo "<Board name=\"Board"$iboard\"" slot=\"$iboard\" type=\"A1821\" description=\"12 Channel 3 kV, 200/20 uA\"></Board>" >> $fout
done

cat "./footer_sim_config.txt" >> $fout
echo "written to "$fout